#!/bin/bash

. ./funcs.sh || exit 1

cd $top

shopt -s nullglob

export LANG=C
export LC_ALL=C
export LANGUAGE=C
export LC_TIME=C
export PATH=/usr/local/bin:/sbin:/usr/sbin:$PATH
kcflags="-Wmaybe-uninitialized"

daily=0
utils=0
misc=0
daily_test=0
no_pm=0
no_pm_sleep=0
no_of=0
no_acpi=0
no_debug_fs=0
sparse=0
smatch=0
spec=0
kerneldoc=0
virtme_64=0
virtme_64_arg=mc
virtme_32=0
virtme_32_arg=mc
virtme_quick=0
virtme_utils_path=$top/v4l-utils/build/usr/bin
virtme_test_media=$top/v4l-utils/contrib/test/test-media
parallel=1
specerr="NO_GIT"
logname=logs
cpus=$CPUS
remote=media_stage
branch=master
bisect=0
patches=0

# For parallel builds use a fourth of the total number of CPUs
p_cpus=$(($CPUS/4))

smatch_git=git://repo.or.cz/smatch.git

while [ ! -z "$1" ]; do
	case "$1" in
	-daily)
		daily=1
		misc=1
		no_pm=1
		no_pm_sleep=1
		no_of=1
		no_acpi=1
		no_debug_fs=1
		sparse=1
		smatch=1
		kerneldoc=1
		utils=1
		spec=1
		virtme_64_arg=all
		logname=daily-logs
		;;
	-daily-test)
		daily=1
		misc=1
		no_pm=1
		no_pm_sleep=1
		no_of=1
		no_acpi=1
		no_debug_fs=1
		sparse=1
		smatch=1
		kerneldoc=1
		utils=1
		spec=1
		virtme_64_arg=all
		daily_test=1
		;;
	-test)
		misc=1
		no_pm=1
		no_pm_sleep=1
		no_of=1
		no_acpi=1
		no_debug_fs=1
		sparse=1
		smatch=1
		kerneldoc=1
		spec=1
		;;
	-misc)
		misc=1
		;;
	-sparse)
		sparse=1
		;;
	-smatch)
		smatch=1
		;;
	-no-pm)
		no_pm=1
		;;
	-no-pm-sleep)
		no_pm_sleep=1
		;;
	-no-of)
		no_of=1
		;;
	-no-acpi)
		no_acpi=1
		;;
	-no-debug-fs)
		no_debug_fs=1
		;;
	-kerneldoc)
		kerneldoc=1
		;;
	-utils)
		utils=1
		;;
	-spec)
		spec=1
		;;
	-patches)
		shift
		patches=$1
		bisect=1
		;;
	-check-patches)
		shift
		patches=$1
		bisect=0
		;;
	-virtme)
		virtme_64=1
		virtme_32=1
		utils=1
		;;
	-virtme-64)
		virtme_64=1
		utils=1
		;;
	-virtme-64-arg)
		shift
		virtme_64_arg=$1
		;;
	-virtme-32)
		virtme_32=1
		utils=1
		;;
	-virtme-32-arg)
		shift
		virtme_32_arg=$1
		;;
	-virtme-quick)
		virtme_quick=1
		utils=0
		if [ ! -f v4l-utils/build/usr/bin/v4l2-compliance-32 ]; then
			utils=1
		fi
		;;
	-virtme-utils-path)
		shift
		virtme_utils_path="$1"
		;;
	-virtme-test-media)
		shift
		virtme_test_media="$1"
		;;
	-sequential)
		parallel=0
		;;
	-cpus)
		shift
		cpus=$1
		p_cpus=$(($cpus/4))
		if [ $p_cpus == 0 ]; then
			p_cpus=1
		fi
		if [ $cpus == 0 ]; then
			cpus=
			p_cpus=
		fi
		;;
	*)
		break
		;;
	esac
	shift
done

if [ "$1" == "clean" ]; then
	rm -rf history logs daily-logs media-git v4l-utils edid-decode smatch virtme-git trees
	exit 0
fi

if [ "$1" == "ccache-clean" ]; then
	rm -rf trees/*/ccache-git
	exit 0
fi

if [ "$1" == "setup" ]; then
	rm -rf history logs daily-logs media-git v4l-utils edid-decode smatch virtme-git
	mkdir -p trees
	cd trees
	mkdir -p $architectures
	cd ..
	git clone git://linuxtv.org/v4l-utils.git
	git clone git://linuxtv.org/edid-decode.git
	git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux-2.6.git media-git
	cd media-git
	git remote add media_tree git://linuxtv.org/media_tree.git
	git remote add media_stage git://linuxtv.org/media_stage.git
	git remote add main $myrepo
	if [ -n "$mylocalrepo" ]; then
		git remote add local $mylocalrepo
	fi
	git remote update
	git checkout -b build-test media_stage/master
	cd ..
	git clone $smatch_git
	cd smatch
	make -j HAVE_LLVM=no
	exit 0
fi

if [ -z "$1" ]; then
	echo 'Usage: build.sh [-patches N] [-test] [-misc] [-sparse] [-smatch]'
	echo '                [-no-pm] [-no-pm-sleep] [-no-of] [-no-acpi] [-no-debug-fs]'
	echo '                [-kerneldoc] [-spec] [-utils] [-sequential] [-cpus N]'
	echo '                [-virtme-64] [-virtme-64-arg ARG] [-virtme-32] [-virtme-32-arg ARG]'
	echo '                [-virtme] [-virtme-quick]'
	#echo '                [-daily] [-daily-test]'
	echo '                arch|all|none [branch [remote]]'
	echo '       build.sh [clean|setup]'
	echo
	echo 'clean:  delete all logs, history and repositories.'
	echo 'ccache-clean: delete trees/*/ccache-git.'
	echo 'setup:  does the same as clean and then sets up all the repositories'
	echo '        from scratch.'
	echo
	echo '-test: enables all options except -utils and -virtme.'
	echo '-misc: enables several miscellaneous tests.'
	echo '-patches N: add the last N patches from the branch one by one and check if'
	echo '            it builds on x86_64 to prevent compile breakage.'
	echo '            Also run checkpatch over each patch. By default N=0.'
	echo '-check-patches N: same as -patches, but skip the compilation.'
	echo '            So just run checkpatch over each patch. By default N=0.'
	echo '-no-pm: turns on the x86_64 CONFIG_PM=n build for the test run.'
	echo '-no-pm-sleep: turns on the x86_64 CONFIG_PM_SLEEP=n build for the test run.'
	echo '-no-of: turns on the x86_64 CONFIG_OF=n build for the test run.'
	echo '-no-acpi: turns on the x86_64 CONFIG_ACPI=n build for the test run.'
	echo '-no-debug-fs turns on the x86_64 CONFIG_DEBUG_FS=n build for the test run.'
	echo '-sparse: turns on the x86_64 sparse build for the test run.'
	echo '-smatch: turns on the x86_64 smatch build for the test run.'
	echo '-kerneldoc: checks kerneldoc of all media headers.'
	echo '-spec: build media specification docs.'
	echo '-utils: build v4l-utils and edid-decode.'
	echo '-virtme: shorthand for -virtme-64 -virtme-32.'
	echo '-virtme-64: build a virtme kernel and run the regression tests in 64 bit.'
	echo '-virtme-64-arg: the argument to the test-media script (default is 'mc').'
	echo '-virtme-32: build a virtme kernel and run the regression tests in 32 bit.'
	echo '-virtme-32-arg: the argument to the 32-bit test of test-media script (default is 'mc').'
	echo '-virtme-quick: skip building the virtme kernel, just run the regression tests.'
	echo '-virtme-utils-path: where to find the v4l-utils utilities, by default use what -utils built.'
	echo '-virtme-test-media: the test-media script to run. By default use v4l-utils/contrib/test/test-media.'
	echo '-cpus N: use -jN for make when building git trees. If N is 0, then use -j only.'
	echo '         The parallel builds will use -j N/4. By default N is the number of'
	echo '         CPUs reported in /proc/cpuinfo.'
	echo '-sequential: build all architectures/configs sequentially instead of in parallel.'
	#echo '-daily: builds against all the specified architectures, builds all'
	#echo '        utilities, the special configs and the sparse/smatch builds and mails'
	#echo '        the results to the mailinglist.'
	#echo '-daily-test: does the same as -daily, except it sends the results to the builder only,'
	#echo '        not to the mailinglist.'
	echo
	echo 'arch|all|none: either specify a specific arch, do them all or do nothing.'
	echo
	echo 'Available architectures: '$architectures
	echo
	echo "branch: the branch to use. Default is 'master' with 'media_stage' as the remote."
	echo "remote: the remote to use. Default is 'main'."
	exit 1
fi

logdir=$top/$logname

if [ $parallel == 0 ]; then
	p_cpus=$cpus
fi

if [ "$1" = "all" ]; then
	archs="$architectures"
elif [ "$1" = "none" ]; then
	archs=""
else
	archs=$1
	if [ ! -d trees/$archs ]; then
		echo unknown arch $archs
		exit 1
	fi
fi
shift
if [ -n "$1" ]; then
	branch="$1"
	remote=main
	shift
fi
if [ -n "$1" ]; then
	remote="$1"
fi

echo Building $remote/$branch for these architectures: $archs

rm -rf $logdir
mkdir -p $logdir

if [ $utils == 1 ]; then
	cd $top/v4l-utils
	git pull
	cd $top/edid-decode
	git pull
fi

cd $top/media-git
rm -f *.patch
rm -rf Documentation/output Documentation/media
git remote update
git reset --hard
git checkout master
git pull
git branch -D build-test
if [ $daily == 1 ]; then
	remote=media_stage
	branch=master
fi
git checkout -b build-test "$remote/$branch" || exit -1

DOCHDRS=include/uapi/linux/lirc.h
DOCHDRS="$DOCHDRS include/uapi/linux/videodev2.h"
DOCHDRS="$DOCHDRS include/uapi/linux/ivtv*.h"
DOCHDRS="$DOCHDRS include/uapi/linux/max2175.h"
DOCHDRS="$DOCHDRS include/uapi/linux/media*.h"
DOCHDRS="$DOCHDRS include/uapi/linux/v4l2*.h"
DOCHDRS="$DOCHDRS include/uapi/linux/uvcvideo.h"
DOCHDRS="$DOCHDRS include/uapi/linux/xilinx-v4l2-controls.h"
DOCHDRS="$DOCHDRS include/uapi/linux/ccs.h"
DOCHDRS="$DOCHDRS include/uapi/linux/smiapp.h"
DOCHDRS="$DOCHDRS include/uapi/linux/cec*.h"
DOCHDRS="$DOCHDRS "`find include/media`
DOCHDRS="$DOCHDRS "`find include/uapi/linux/dvb`
DOCHDRS="$DOCHDRS "`find drivers/media/|grep \\\.h$`
DOCHDRS="$DOCHDRS "`find drivers/staging/media/|grep -v atomisp|grep \\\.h$`


cd $top

if [ $sparse == 1 -o $smatch == 1 -o ! -d smatch ]; then
	if [ ! -d smatch ]; then
		git clone $smatch_git
	fi
	cd smatch
	git remote update
	git reset --hard origin/master
	make -j HAVE_LLVM=no
	cd ..
fi

export CCACHE_MAXSIZE=512M

logdate=`date +%Y%m%d-%H%M`.log
logday=`date +%A`

echo -n "date:			" >$logdir/summary
date >>$logdir/summary
echo "media-tree git repo:	$myrepo" >>$logdir/summary
echo "media-tree git branch:	$remote/$branch" >>$logdir/summary
cur_branch=`cd $top/media-git; git symbolic-ref --short -q HEAD`
cur_hash=`cd $top/media-git; git show-ref -s refs/heads/$cur_branch`
media_hash=$cur_hash
echo "media-tree git hash:	$cur_hash" >$logdir/hashes
cur_hash=`cd $top/v4l-utils; git show-ref -s refs/heads/master`
echo "v4l-utils git hash:	$cur_hash" >>$logdir/hashes
cur_hash=`cd $top/edid-decode; git show-ref -s refs/heads/master`
echo "edid-decode git hash:	$cur_hash" >>$logdir/hashes
echo -n "gcc version:		" >>$logdir/hashes
$top/cross/bin/i686-linux/bin/i686-linux-gcc --version | head -1 >>$logdir/hashes
if [ $use_ccache = "1" ]; then
	echo -n "ccache version:		" >>$logdir/hashes
	/usr/bin/ccache --version | head -1 >>$logdir/hashes
fi
echo    "smatch/sparse repo:     $smatch_git" >>$logdir/hashes
echo -n "smatch version:		" >>$logdir/hashes
$top/smatch/smatch --version >>$logdir/hashes
echo -n "sparse version:		" >>$logdir/hashes
$top/smatch/sparse --version >>$logdir/hashes
echo    "build-scripts repo:     https://git.linuxtv.org/hverkuil/build-scripts.git" >>$logdir/hashes
cur_hash=`cd $top; git show-ref -s refs/heads/master`
echo    "build-scripts git hash: $cur_hash" >>$logdir/hashes
echo -n "host hardware:		" >>$logdir/hashes
uname -m >>$logdir/hashes
echo -n "host os:		" >>$logdir/hashes
uname -r >>$logdir/hashes
cat $logdir/hashes >>$logdir/summary
echo >>$logdir/summary
cp $logdir/summary $logdir/header

# Somehow this file gets generated occasionally. Make sure it is
# removed.
rm -f trees/Module.symvers
rm -f media-git/include/uapi/asm media-git/include/asm
rm -f $logdir/strcpy.log
touch $logdir/strcpy.log
rm -f $logdir/patch-warns.log

cd $top/media-git

trap 'kill $(jobs -p) >/dev/null 2>&1' EXIT

if [ $patches -gt 0 ]; then
	git format-patch -M HEAD~$patches

	fail=0
	for p in *.patch; do
		if ! `grep -qi "^Signed-off-by: $name <$email>" $p` ; then
			echo ERROR: $p: Missing Signed-off-by: "$name <$email>"
			fail=1
		fi
		if `grep -qi '^Change-Id:' $p` ; then
			echo
			echo "ERROR: $p: Don't use Change-Id"
			fail=1
		fi
		if ! `grep -qi '^Fixes:' $p` ; then
			if `grep -qi '^cc:.*stable' $p` ; then
				echo
				echo "ERROR: $p: Cc to stable, but no Fixes: tag"
				fail=1
			fi
		fi
		if `grep -qi '^Cc:.*linux-media' $p` ; then
			echo
			echo "ERROR: $p: Don't Cc linux-media"
			fail=true
		fi
	done
	if [ $fail == 1 ]; then
		exit -1
	fi

	scripts/checkpatch.pl --strict *.patch | tee $logdir/checkpatch.log

	fail=0
	if grep -i ".Signed-off-by" $logdir/checkpatch.log; then
		fail=1
	fi
	if grep "Unknown commit id '" $logdir/checkpatch.log; then
		fail=1
	fi
	if [ $fail == 1 ]; then
		exit -1
	fi

	if [ $bisect == 1 ]; then
		echo
		echo Sequentially build $patches patches...
		echo

		git reset --hard HEAD~$patches

		newconfig=1
		arch=x86_64
		export CCACHE_DIR=$top/trees/$arch/ccache-git
		mkdir -p $CCACHE_DIR
		for p in *.patch; do
			if [ $newconfig == 1 ]; then
				setup_config $arch $arch
			fi
			make $opts W=1 KCFLAGS="$kcflags" O=../trees/$arch/media-git -j$cpus drivers/staging/media/ drivers/media/ drivers/input/touchscreen/ || exit -1
			patches=$((patches-1))
			echo
			echo
			echo
			echo Test with patch $p
			echo
			echo
			echo
			git reset --hard "$remote/$branch" >/dev/null
			git reset --hard HEAD~$patches
			newconfig=0
			if egrep -q /Kconfig $p; then
				# Only recreate the kernel config if Kconfigs were touched
				# in the applied patch
				newconfig=1
			fi
			echo
			echo
			echo
			echo
		done
	fi

	echo
	for p in *.patch; do
		if ! `grep -qi "^Subject:.*[^a-zA-Z_0-9]media:" $p` ; then
			echo "WARNING: $p: Missing 'media:' prefix in Subject" | tee -a $logdir/patch-warns.log
			fail=1
		fi
		if `grep -qi "^diff .*/boot/dts/" $p` ; then
			echo WARNING: $p: Device tree changes should not be part of the media subsystem | tee -a $logdir/patch-warns.log
		fi
		if ! `grep -qi '^cc:.*stable' $p` ; then
			if `grep -qi '^Fixes:' $p` ; then
				echo
				echo WARNING: $p: Fixes: tag, but no Cc to stable | tee -a $logdir/patch-warns.log
				fail=1
			fi
		fi
		if `grep -qi '^Cc:.*hverkuil' $p` ; then
			echo
			echo "WARNING: $p: Don't Cc Hans Verkuil" | tee -a $logdir/patch-warns.log
			fail=true
		fi
		if `grep -qi '^Cc:.*mchehab' $p` ; then
			echo
			echo "WARNING: $p: Don't Cc Mauro Carvalho Chehab" | tee -a $logdir/patch-warns.log
			fail=true
		fi
	done
fi

for arch in $archs; do
	(
	cd $top/media-git

	setup_arch $arch

	echo
	echo
	echo
	echo Building for $arch...
	echo
	echo
	echo
	export CCACHE_DIR=$top/trees/$arch/ccache-git
	mkdir -p $CCACHE_DIR
	date >$logdir/linux-git-$arch.log
	setup_config $arch $arch
	cp ../trees/$arch/media-git/.config $logdir/$arch.config
	(make $opts W=1 KCFLAGS="$kcflags" O=../trees/$arch/media-git -j$p_cpus drivers/staging/media/ drivers/media/ drivers/input/touchscreen/ && echo End git build) 2>&1 | strip_top | tee -a $logdir/linux-git-$arch.log
	date >>$logdir/linux-git-$arch.log
	echo -n "linux-git-$arch: " >>$logdir/summary
	perl -f $top/parselog.pl <$logdir/linux-git-$arch.log >>$logdir/summary
	) &
	if [ $parallel == 0 ]; then
		wait
	fi
done
if [ $parallel == 1 ]; then
	wait
fi

configs=
if [ $no_pm -eq 1 ]; then
	configs="$configs no-pm"
fi
if [ $no_pm_sleep -eq 1 ]; then
	configs="$configs no-pm-sleep"
fi
if [ $no_of -eq 1 ]; then
	configs="$configs no-of"
fi
if [ $no_acpi -eq 1 ]; then
	configs="$configs no-acpi"
fi
if [ $no_debug_fs -eq 1 ]; then
	configs="$configs no-debug-fs"
fi

cd $top/media-git
arch=x86_64
setup_arch $arch

for conf in $configs; do
	(
	echo
	echo
	echo
	echo arch $arch kernel git with $conf.config
	echo
	echo
	echo

	mkdir -p ../trees/$conf
	rm -rf ../trees/$conf/media-git
	mkdir ../trees/$conf/media-git
	export CCACHE_DIR=$top/trees/$arch/ccache-git
	mkdir -p $CCACHE_DIR

	date >$logdir/$conf.log
	make mrproper
	cp $top/configs/$conf.config ../trees/$conf/media-git/.config
	make $opts O=../trees/$conf/media-git olddefconfig
	make $opts O=../trees/$conf/media-git prepare
	cp ../trees/$conf/media-git/.config $logdir/$conf.config
	(make $opts O=../trees/$conf/media-git -j$p_cpus W=1 KCFLAGS="$kcflags" drivers/media/ drivers/staging/media/ && echo End git build) 2>&1 | strip_top | tee -a $logdir/$conf.log
	date >>$logdir/$conf.log
	echo -n "$conf.config: " >>$logdir/summary
	perl -f $top/parselog.pl <$logdir/$conf.log >>$logdir/summary
	) &
	if [ $parallel == 0 ]; then
		wait
	fi
done
if [ $parallel == 1 ]; then
	wait
fi

if [ $sparse -eq 1 ]; then
	arch=i686
	conf=sparse
	echo
	echo
	echo
	echo arch $arch sparse build
	echo
	echo
	echo
	cd $top/media-git
	setup_arch $arch
	export CCACHE_DIR=$top/trees/$arch/ccache-git
	mkdir -p $CCACHE_DIR
	date >$logdir/$conf.log
	setup_config $arch $conf
	(make $opts O=../trees/$conf/media-git -i -j$cpus W=1 C=1 CHECK=$top/smatch/sparse CF="-D__CHECK_ENDIAN__ -fmemcpy-max-count=11000000" KCFLAGS="$kcflags" drivers/media/ drivers/staging/media/ && echo End git build) 2>&1 | strip_top | tee -a $logdir/$conf.log
	date >>$logdir/$conf.log
	echo -n "$conf: " >>$logdir/summary
	perl -f $top/parselog.pl <$logdir/$conf.log >>$logdir/summary
fi

if [ $smatch -eq 1 ]; then
	arch=x86_64
	conf=smatch
	echo
	echo
	echo
	echo arch $arch smatch build
	echo
	echo
	echo
	cd $top/media-git
	setup_arch $arch
	export CCACHE_DIR=$top/trees/$arch/ccache-git
	mkdir -p $CCACHE_DIR
	date >$logdir/$conf.log
	setup_config $arch $conf
	(make $opts O=../trees/$conf/media-git -i -j$cpus W=1 C=1 CHECK="$top/smatch/smatch -p=kernel" KCFLAGS="$kcflags" drivers/media/ drivers/staging/media/ && echo End git build) 2>&1 | strip_top | tee -a $logdir/$conf.log
	date >>$logdir/$conf.log
	echo -n "$conf: " >>$logdir/summary
	perl -f $top/parselog.pl <$logdir/$conf.log >>$logdir/summary
fi

cd $top/media-git

if [ $misc == 1 ]; then
	echo Check COMPILE_TEST...
	echo -n "COMPILE_TEST: " >>$logdir/summary
	fails=""
	make mrproper
	make ARCH=x86_64 allyesconfig >/dev/null && for i in  $(grep "config " $(find drivers/staging/media/ -name Kconfig) $(find drivers/media/ -name Kconfig) | grep -v "\#.*Kconfig" | grep -v MEDIA_HIDE_ANCILLARY_SUBDRV | grep -v VIDEO_TEGRA_TPG | cut -d' ' -f 2) ; do if [ "$(grep $i .config)" == "" ]; then fails="$fails $i"; fi; done
	if [ -z "$fails" ]; then
		echo OK >>$logdir/summary
	else
		echo WARNINGS:$fails >>$logdir/summary
	fi

	echo Check for strcpy/strncpy/strlcpy...
	echo -n "strcpy/strncpy/strlcpy: " >>$logdir/summary

	if git grep -q 'str[nl]\?cpy[^_]' drivers/staging/media/ drivers/media/ ; then
		git grep 'str[nl]\?cpy[^_]' drivers/staging/media/ drivers/media/ >$logdir/strcpy.log
		strcpy=`git grep 'strcpy[^_]' drivers/media/ drivers/staging/media/|wc -l`
		strlcpy=`git grep 'strlcpy[^_]' drivers/media/ drivers/staging/media/|wc -l`
		strncpy=`git grep 'strncpy[^_]' drivers/media/ drivers/staging/media/|wc -l`
		echo "WARNINGS: found $strcpy strcpy(), $strncpy strncpy(), $strlcpy strlcpy()" >>$logdir/summary
	else
		echo OK >>$logdir/summary
	fi

	cd $top

	echo Check for ABI changes...
	cat >abi-test.c <<'EOF'
#include <linux/time.h>
#include <linux/string.h>
typedef unsigned long           uintptr_t;
#include <stdbool.h>
#include <stddef.h>
#include <linux/videodev2.h>
#include <linux/v4l2-subdev.h>
#include <linux/media.h>
#define memset(a, b, c)
#include <linux/cec.h>
#include <linux/lirc.h>
#include <linux/dvb/audio.h>
#include <linux/dvb/ca.h>
#include <linux/dvb/dmx.h>
#include <linux/dvb/frontend.h>
#include <linux/dvb/net.h>
#include <linux/dvb/osd.h>
#include <linux/dvb/video.h>

union {
EOF
	cat media-git/include/uapi/linux/videodev2.h media-git/include/uapi/linux/v4l2-subdev.h media-git/include/uapi/linux/media.h media-git/include/uapi/linux/cec.h media-git/include/uapi/linux/lirc.h media-git/include/uapi/linux/dvb/*.h | perl -ne 'if (m/\#define\s+([A-Z][A-Z0-9_]+)\s+\_IO.+\(.*\,\s*([^\)]+)\)/) {print "$2 t_$2;\n"}' | egrep -v '^(int |unsigned |__)' | sed 's/t_struct /t_/' | sort | uniq >>abi-test.c
	echo '} x;' >>abi-test.c
	( cd media-git/include/uapi; ln -sf asm-generic asm; cd ..; ln -sf asm-generic asm )

	fail=false
	fail_result=""
	for arch in $architectures; do
		setup_arch $arch
		$top/cross/bin/$cross/bin/$cross-gcc -g -Og -c abi-test.c -D__KERNEL__ -D__DVB_CORE__ -I media-git/include/uapi -I trees/$arch/media-git/include/generated/uapi -I media-git/include
		abi-dumper abi-test.o -o $logdir/abi.$arch.dump
		if [ ! -s $logdir/abi.$arch.dump ]; then
			echo rm -f $logdir/abi.$arch.dump
		else
			if [ ! -f $top/abi/abi.$arch.dump ]; then
				cp $logdir/abi.$arch.dump $top/abi/abi.$arch.dump
			fi
			if ! abi-compliance-checker -s -l abi-test.o -old abi/abi.$arch.dump -new $logdir/abi.$arch.dump; then
				fail_result="$fail_result $arch"
				fail=true
			fi
			mv compat_reports/abi-test.o/X_to_Y/compat_report.html $logdir/compat_reports.$arch.html
		fi
		#rm -f abi-test.o
	done
	if $fail; then
		echo abi-compliance: ABI WARNING: changed for$fail_result >>$logdir/summary
	else
		echo abi-compliance: ABI OK >>$logdir/summary
	fi
	rm -f media-git/include/uapi/asm media-git/include/asm

	fail=false
	if ! ./pahole.sh ; then
		echo pahole: ABI ERROR: >>$logdir/summary
		fail=true
	else
		echo pahole: ABI OK >>$logdir/summary
	fi | tee $logdir/pahole.log
	if $fail; then
		echo >>$logdir/summary
		grep -v "^Install" $logdir/pahole.log >>$logdir/summary
		echo >>$logdir/summary
	fi
fi

# v4l-utils/edid-decode build
if [ $utils -eq 1 ]; then
	echo Build v4l-utils...
	date >$logdir/utils.log
	cd $top/v4l-utils
	git clean -f
	rm -rf build usr
	meson setup -Dv4l2-compliance-32=true -Dv4l2-ctl-32=true -Dprefix=$top/v4l-utils/build/usr -Dudevdir=$top/v4l-utils/build/usr/lib/udev -Dsystemdsystemunitdir=$top/v4l-utils/build/usr/lib/systemd build/
	ninja -C build/ | grep -v "strerrorname" | grep -v gethostbyname | strip_top | tee -a $logdir/utils.log
	ninja -C build install | strip_top | tee -a $logdir/utils.log

	echo Build edid-decode...
	cd $top/edid-decode
	git clean -f
	rm -rf build build-wasm
	meson setup build 2>&1 | strip_top | tee -a $logdir/utils.log
	ninja -C build 2>&1 | strip_top | tee -a $logdir/utils.log
	meson setup build-wasm --cross-file ./emscripten/wasm-crossfile.txt --prefix=$(pwd)/install-wasm 2>&1 | strip_top | tee -a $logdir/utils.log
	meson install -C build-wasm 2>&1 | strip_top | tee -a $logdir/utils.log

	date >>$logdir/utils.log
	echo -n "utils: " >>$logdir/summary
	fail=true
	if grep -qi error $logdir/utils.log ; then
		echo ERRORS: >>$logdir/summary
	elif grep -qi warning $logdir/utils.log ; then
		echo WARNINGS: >>$logdir/summary
	else
		echo OK >>$logdir/summary
		fail=false
	fi
	if $fail; then
		echo >>$logdir/summary
		egrep '(warning)|(error)' -i $logdir/utils.log >>$logdir/summary
		echo >>$logdir/summary
	fi
fi

specerr="NO_GIT"

if [ $spec -eq 1 ]; then
	cd $top/media-git
	echo Build media spec...
	rm -rf Documentation/output Documentation/media
	sed s,userspace-api/media,media/userspace-api,g -i Documentation/Makefile
	mkdir -p Documentation/media
	cat <<END >Documentation/media/index.rst
.. SPDX-License-Identifier: GPL-2.0

.. include:: <isonum.txt>

**Copyright** |copy| 1991-: LinuxTV Developers

================================
Linux Kernel Media Documentation
================================

.. toctree::
	:maxdepth: 4

        userspace-api/index
        driver-api/index
        admin-guide/index
END

	rsync -vuza --delete Documentation/admin-guide/media/ Documentation/media/admin-guide
	rsync -vuza --delete Documentation/driver-api/media/ Documentation/media/driver-api
	rsync -vuza --delete Documentation/userspace-api/media/ Documentation/media/userspace-api
	date >$logdir/spec-html.log
	make SPHINXDIRS="media" htmldocs 2>&1 | strip_top >>$logdir/spec-html.log
	date >>$logdir/spec-html.log

	date >$logdir/spec-pdf.log
	( make SPHINXDIRS=media pdfdocs </dev/null && make SPHINXDIRS=media pdfdocs </dev/null && echo PDF OK ) 2>&1 | strip_top >>$logdir/spec-pdf.log
	date >>$logdir/spec-pdf.log

	git checkout Documentation/Makefile

	fail=true
	if grep -q 'ERROR:' $logdir/spec-html.log ; then
		specerr="ERRORS:"
	elif grep -q 'WARNING:' $logdir/spec-html.log ; then
		specerr="WARNINGS:"
	else
		specerr="OK"
		fail=false
	fi
	echo "spec-html: $specerr" >>$logdir/summary
	if $fail; then
		echo >>$logdir/summary
		egrep '(WARNING:)|(ERROR:)' $logdir/spec-html.log >>$logdir/summary
		echo >>$logdir/summary
	fi

	specerr="ERRORS"
	if grep -q 'PDF OK' $logdir/spec-pdf.log ; then
		specerr="OK"
	fi
	echo "spec-pdf: $specerr" >>$logdir/summary
fi

if [ $kerneldoc -eq 1 ]; then
	cd $top/media-git
	echo Check kerneldoc...
	date >$logdir/kerneldoc.log
	for i in $DOCHDRS; do
		scripts/kernel-doc -none "$i" 2>&1 | strip_top >>$logdir/kerneldoc.log
	done
	date >>$logdir/kerneldoc.log
	echo -n "kerneldoc: " >>$logdir/summary
	fail=true
	if grep -qi error: $logdir/kerneldoc.log ; then
		echo ERRORS: >>$logdir/summary
	elif grep -qi warning: $logdir/kerneldoc.log ; then
		echo WARNINGS: >>$logdir/summary
	else
		echo OK >>$logdir/summary
		fail=false
	fi
	if $fail; then
		echo >>$logdir/summary
		egrep '(warning:)|(error:)' -i $logdir/kerneldoc.log >>$logdir/summary
		echo >>$logdir/summary
	fi
fi

if [ $virtme_64 -eq 1 -o $virtme_32 -eq 1 ]; then
	echo >>$logdir/summary
	echo -n "date:			" >>$logdir/summary
	date >>$logdir/summary

	# git virtme build
	cd $top/media-git
	setup_arch x86_64
	conf=virtme

	if [ $virtme_quick == 0 ]; then
		echo
		echo
		echo
		echo arch x86_64 kernel git with $conf.config
		echo
		echo
		echo

		mkdir -p ../trees/$conf
		rm -rf ../trees/$conf/media-git
		mkdir ../trees/$conf/media-git
		export CCACHE_DIR=$top/trees/$conf/ccache-git
		mkdir -p $CCACHE_DIR

		date >$logdir/$conf.log
		make mrproper
		cp $top/configs/$conf.config ../trees/$conf/media-git/.config
		make $opts O=../trees/$conf/media-git olddefconfig
		make $opts O=../trees/$conf/media-git prepare
		cp ../trees/$conf/media-git/.config $logdir/$conf.config
		make $opts O=../trees/$conf/media-git -j$cpus 2>&1 | strip_top | tee $logdir/$conf.log
		make -j$cpus O=../trees/$conf/media-git INSTALL_MOD_PATH=$top/trees/$conf/media-git/virtme-modules modules_install 2&>1 | strip_top | tee -a $logdir/$conf.log
		date >>$logdir/$conf.log
	fi

	cd ../trees/$conf/media-git
	rm -f $log/test-media-dmesg.log
	if [ $virtme_64 -eq 1 -a -n "$virtme_64_arg" ]; then
		echo
		echo
		echo
		echo Running test-media -kmemleak $virtme_64_arg
		echo
		echo
		echo
		timeout --foreground -s INT 30m vng -r . -v --force-9p --cwd $top --rwdir $top -e "./virtme-test.sh "$virtme_utils_path" "$virtme_test_media" -kmemleak $virtme_64_arg" --user root --memory 4G --cpus 2 -o=-no-reboot 2>&1 | tee $logdir/$logday-test-media-64.log
		result=${PIPESTATUS[0]}
		if [ $result -eq 124 ]; then
			echo "qemu timed out!" >>$logdir/$logday-test-media-64.log
		fi
		if [ -f $top/test-media-dmesg.log ]; then
			mv $top/test-media-dmesg.log $logdir/$logday-test-media-64-dmesg.log
		fi

		echo -n "virtme-64: " >>$logdir/summary
		virtme_result=`grep "^Final " $logdir/$logday-test-media-64.log`
		if [ -z "$virtme_result" ]; then
			echo ERRORS >>$logdir/summary
		elif echo $virtme_result | grep -v "Failed: 0" ; then
			echo "ERRORS: $virtme_result" >>$logdir/summary
		elif echo $virtme_result | grep -v "Warnings: 0" ; then
			echo "WARNINGS: $virtme_result" >>$logdir/summary
		else
			echo "OK: $virtme_result" >>$logdir/summary
		fi
	fi
	rm -f $log/test-media-dmesg.log
	if [ $virtme_32 -eq 1 -a -n "$virtme_32_arg" ]; then
		echo
		echo
		echo
		echo Running test-media -kmemleak -32 $virtme_32_arg
		echo
		echo
		echo
		timeout --foreground -s INT 30m vng -r . -v --force-9p --cwd $top --rwdir $top -e "./virtme-test.sh "$virtme_utils_path" "$virtme_test_media" -kmemleak -32 $virtme_32_arg" --user root --memory 4G --cpus 2 -o=-no-reboot 2>&1 | tee $logdir/$logday-test-media-32.log
		result=${PIPESTATUS[0]}
		if [ $result -eq 124 ]; then
			echo "qemu timed out!" >>$logdir/$logday-test-media-32.log
		fi
		if [ -f $top/test-media-dmesg.log ]; then
			mv $top/test-media-dmesg.log $logdir/$logday-test-media-32-dmesg.log
		fi

		echo -n "virtme-32: " >>$logdir/summary
		virtme_result=`grep "^Final " $logdir/$logday-test-media-32.log`
		if [ -z "$virtme_result" ]; then
			echo ERRORS >>$logdir/summary
		elif echo $virtme_result | grep -v "Failed: 0" ; then
			echo "ERRORS: $virtme_result" >>$logdir/summary
		elif echo $virtme_result | grep -v "Warnings: 0" ; then
			echo "WARNINGS: $virtme_result" >>$logdir/summary
		else
			echo "OK: $virtme_result" >>$logdir/summary
		fi
	fi

fi

echo >>$logdir/summary

if [ -f $logdir/patch-warns.log ]; then
	echo "patch warnings:" >>$logdir/summary
	echo >>$logdir/summary
	cat $logdir/patch-warns.log >>$logdir/summary
	echo >>$logdir/summary
fi

echo -n "date:			" >>$logdir/summary
date >>$logdir/summary

echo
echo
echo
echo ------------------------------------------------------
echo

cd $top

if grep -q "^[^ ]*: ERRORS" $logdir/summary ; then
	result=ERRORS
elif grep -q "^[^ ]*: ABI ERROR" $logdir/summary ; then
	result="ABI ERROR"
elif grep -q "^[^ ]*: ABI WARNING" $logdir/summary ; then
	result="ABI WARNING"
elif grep -q "^[^ ]*: WARNINGS" $logdir/summary ; then
	result=WARNINGS
else
	result=OK
fi

if [ $daily -eq 1 -a -f ./env-upload.sh ]; then
	. ./upload.sh
else
	cat $logdir/summary
	echo
	echo Overall build result: $result
	echo
fi
