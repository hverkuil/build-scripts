#!/bin/bash

export top=$PWD

utils_path="$1"
shift
test_media="$1"
shift

export PATH=.:$utils_path:$PATH

arg=$*

echo
echo -----------------------------------------------------------
echo

echo running $test_media $arg | tee /dev/kmsg
echo v4l-utils path: $utils_path
echo
$test_media $arg

dmesg -n notice

echo
echo finished running tests | tee /dev/kmsg

dmesg -T >$top/test-media-dmesg.log

echo b >/proc/sysrq-trigger
