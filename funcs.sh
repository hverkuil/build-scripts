#!/bin/bash

top=$PWD

if [ ! -f env.sh ]; then
	echo The file env.sh was not found in the current directory $top.
	exit 1
fi

. ./env.sh

if [ "$use_ccache" != "0" ]; then
	if [ -x /usr/bin/ccache ]; then
		use_ccache=1
	else
		use_ccache=0
	fi
fi

function setup_arch()
{
	a=$1
	cross=$a-linux

	case $a in
	i686)
		a=i386
		;;
	x86_64) 
		;;
	powerpc64)
		a=powerpc
		;;
	arm)
		cross=arm-linux-gnueabi
		;;
	arm64)
		a=arm64
		cross=aarch64-linux-gnu
		;;
	esac
	export PATH=$top/cross/bin/$cross/bin:$PATH
	if [ $use_ccache = "1" ]; then
		export PATH=$top/cross/ccache:$PATH
	fi
	opts="ARCH=$a CROSS_COMPILE=$cross- -Oline"
}

function setup_config()
{
	arch=$1
	conf=$2

	cd $top/media-git
	rm -rf ../trees/$conf/media-git
	mkdir ../trees/$conf/media-git
	make mrproper
	make $opts O=../trees/$conf/media-git allyesconfig
	cd ../trees/$conf/media-git
	if [ -f ../../../configs/$conf.config ]; then
		cp ../../../configs/$conf.config .config
	elif [ $arch = x86_64 -o $arch = i686 ]; then
		perl -p -i -e 's/CONFIG_STAGING_EXCLUDE_BUILD=y/CONFIG_STAGING_EXCLUDE_BUILD=n/' .config
		perl -p -i -e 's/CONFIG_KCOV=y/CONFIG_KCOV=n/' .config
		perl -p -i -e 's/CONFIG_WERROR=y/CONFIG_WERROR=n/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_(.*)=y/CONFIG_TOUCHSCREEN_\1=n/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_SUR40=n/CONFIG_TOUCHSCREEN_SUR40=y/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_ATMEL_MXT=n/CONFIG_TOUCHSCREEN_ATMEL_MXT=y/' .config
		perl -p -i -e 's/CONFIG_TOUCHSCREEN_ATMEL_MXT_T37=n/CONFIG_TOUCHSCREEN_ATMEL_MXT_T37=y/' .config
		( yes n | make $opts olddefconfig >/dev/null 2>&1 & ) ; sleep 5; kill `pidof yes` 2>/dev/null
		( yes | make $opts olddefconfig >/dev/null 2>&1 & ) ; sleep 5; kill `pidof yes` 2>/dev/null
	fi
	cd $top/media-git
	make $opts O=../trees/$conf/media-git prepare
}

function strip_top()
{
	sed "s;$top/media-git/;;g"
}

architectures="arm arm64 powerpc64 i686 x86_64"

if [ -z "$CPUS" ]; then
	CPUS=`cat /proc/cpuinfo | grep -i processor | wc -l`
fi
