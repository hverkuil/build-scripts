#!/bin/bash

. ./funcs.sh || exit 1

if [ -z "$1" ]; then
	echo "Usage: gitmenu.sh arch|all|no"
	echo
	echo "arch|all|no: update the .config by calling 'make menuconfig' for the"
	echo "    specified architecture or all. Use 'no' to update just the no-<something> configs."
	exit 0
fi

arch=$1
config=$1
if [ "$arch" = "virtme" -o "$arch" = "no-pm" -o "$arch" = "no-pm-sleep" -o "$arch" = "no-of" -o "$arch" = "no-acpi" -o "$arch" == "no-debug-fs" ]; then
	arch=x86_64
fi

if [ "$arch" = "no" ]; then
	for arch in no-pm no-pm-sleep no-of no-acpi no-debug-fs; do
		if [ -f configs/$arch.config ]; then
			./gitmenu.sh $arch
		fi
	done
	exit 0
fi

if [ "$arch" = "all" ]; then
	archs=`(cd trees; echo *)`
	for arch in $archs virtme no-pm no-pm-sleep no-of no-acpi no-debug-fs; do
		if [ -f configs/$arch.config ]; then
			./gitmenu.sh $arch
		fi
	done
	exit 0
fi

if [ ! -f configs/$config.config ]; then
	echo cannot find configs/$config.config
	exit 1
fi

setup_arch $arch
opts="ARCH=$a CROSS_COMPILE=$cross-"

case $config in
no-pm)
	check=CONFIG_PM
	;;
no-pm-sleep)
	check=CONFIG_PM_SLEEP
	;;
no-of)
	check=CONFIG_OF
	;;
no-acpi)
	check=CONFIG_ACPI
	;;
no-debug-fs)
	check=CONFIG_DEBUG_FS
	;;
*)
	check=CONFIG_DUMMY_VALUE
	;;
esac

cd media-git
make -j$CPUS mrproper
cd ..
cp configs/$config.config trees/$arch/media-git/.config
echo -------------------------------------------
echo
echo Editing $config.config
echo
echo -------------------------------------------
make -j$CPUS -C trees/$arch/media-git $opts menuconfig &&
make -j$CPUS -C trees/$arch/media-git $opts prepare &&
! grep -q "$check=[my]" trees/$arch/media-git/.config &&
cp trees/$arch/media-git/.config configs/$config.config
echo -------------------------------------------
echo
echo Finished $config.config
echo
echo -------------------------------------------

if grep -q "$check=[my]" trees/$arch/media-git/.config ; then
	echo
	echo ERROR: $check is present in trees/$arch/media-git/.config:
	echo
	grep "$check=[my]" trees/$arch/media-git/.config
	exit -1
fi
