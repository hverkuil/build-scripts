while (<>) {
	last if /checksyscalls/;
}

while (<>) {
	last if /^  CC/;
}

$errors = "";
$keep = 1;
$result = "ERRORS:";

while (<>) {
	if (/VDSOSYM/) {
		$errors = "";
		$err = "";
	}

	if (/End git build/) {
		$errors .= $err if $keep;
		if (length($errors)) {
			$result = "WARNINGS:";
		}
		else {
			$result = "OK";
		}
		last;
	}
	$lines .= $_;
	if (/^  CC/ || /^  LD/ || /^  AR/ || /^  HOSTCC/ || /^  LEX/ || /^  YACC/ ||
	    /^  MKELF/ || /^  UPD/ || /^  HOSTLD/ || /^  CHECK/ || /^make/ || /^  VDSO/ || /^  OBJCOPY/) {
		$errors .= $err if $keep;
		$keep = 1;
		$err = "";
		next;
	}
	# The following messages are driver-specific for warnings
	# we can't do anything about.
	if (/(cx23885-dvb.c|cx88-dvb.c|saa7134-dvb.c|mb86a16.c|solo6x10|mm.h|page-flags.h).*too hairy/ ||
	    /(imgu_dmamap_alloc_buffer|smscore_register_device|__dma_alloc_buffer|ipu6_dma_alloc).*use '.*' here instead of GFP_KERNEL/ ||
	    /(ar0521_power_on|ov5645_set_power_on|tc358746_suspend|__s5c73m3_power_off).*clk_prepare_enable/ ||
	    /(vdec_hevc_start|vdec_1_start).*clk_prepare_enable/ ||
	    /too long token expansion/ || /parse error: OOM:/ ||
	    /turning off implications after/
	   ) {
		$errors .= $err if $keep;
		$keep = 1;
		$err = "";
		next;
	}
	$err .= $_;
}
printf "%s\n", $result;
if (length($errors)) {
	print "\n" . $errors . "\n";
}
